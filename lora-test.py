from flask import Flask, redirect, url_for, request, jsonify, Response, stream_with_context
from configparser import ConfigParser
import psycopg2
from datetime import datetime, timedelta


app=Flask(__name__)

def config(filename='db_connect.ini', section='connections'):
	parser = ConfigParser()
	parser.read(filename)

	db = {}
	if parser.has_section(section):
		params = parser.items(section)
		for param in params:
			db[param[0]] = param[1]
	else:
		raise Exception('Section {0} not found in the {1} file'.format(section, filename))

	return db

def commit_query():
	conn = None
	cur = None
	try:
		params = config()

		conn = psycopg2.connect(**params)
		
		cur = conn.cursor()
		return cur, conn
	   
	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
		return False, False

def get_database(query, payload=None):
	cur, conn = commit_query()
	if cur == False:
		return False

	if payload == None:
		cur.execute(query)
	else:
		cur.execute(query,payload)

	ids = cur.fetchall()
	return ids


@app.route('/get-price', methods=['POST'])
def get_price():
	ticker = request.json['ticker']
	date = request.json['date']
	time_window = request.json['time_window']#days

	time_date = datetime.strptime(date, "%Y-%m-%d")
	delta_time = time_date - timedelta(days=time_window)
	
	delta_str = delta_time.strftime("%Y-%m-%d")

	param = int(delta_str[5:7]) - int(date[5:7])
	if param >= -3:
		print(param)
		print("masih masuk")
	else:
		print(param)
		print("udah kelewatan")
		response = {
			"messages":"Request Authorizations",
			"data":[]
		}
		return response, 401
	
	query = "select * from imported_closes where (day between %s and %s) and ticker = %s;"
	payload = (delta_str, date, ticker)

	get_data = get_database(query, payload)
	
	clean_data = []
	for data in get_data:
		stock = {
			"ticker":data[0],
			"date":data[1],
			"price":data[2]
		}
		if stock not in clean_data:
			clean_data.append(stock)
		else:
			pass

	response = {
		"Messages":"ok",
		"data":clean_data
	}
	return response, 200


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5020)